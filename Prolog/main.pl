:-consult('bc.pl').

%devolve lista de todas as farmacias.
farmacias([
    "farmacia Alves de Oliveira",
    "farmacia Antiga da porta do Olival",
    "farmacia benisa",
    "farmacia Coimbroes",
    "farmacia Couto",
    "farmacia da foz",
    "farmacia da Prelada",
    "farmacia do campo",
    "farmacia do Passeio Alegre",
    "farmacia dos Clerigos",
    "farmacia dos Montes Burgos",
    "farmacia e falcao",
    "farmacia ferreira da silva nortshoping",
    "farmacia ferro",
    "farmacia fonte luminosa",
    "farmacia gramacho",
    "farmacia Lemos",
    "farmacia matosinhos sul",
    "farmacia moderna",
    "farmacia parque",
    "farmacia peninsular",
    "farmacia Sao Joao"
]).

%local do Armazem, inicio e fim das entregas
armazem("rotunda boavista").

% velocidade de deslocamento do veiculo em metros/hora 50 km/h = 50 000 m/h.
velocidade(50000).

% hora de inicio das entregas:
horaInicio(8).

%hora apartir da qual entramos no periodo da tarde.
horaTarde(13).

% encontra todas as ligacoes de um no.
% Devolve as Ligacoes com uma Lista de nos.
findLigacoes(No,Ligacoes):-
    findall(Nos,ligacao(No,Nos,_),Ligacoes).
% test: findLigacoes("castelo queijo",L).


% Devolve a Distancia em recta entre 2 nos (No1 e No2) em metros.
distancia(No1,No2,Distancia):-
    no(No1,Lat1,Lon1),no(No2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist),
    Dist2 is Dist * 1000,
    Distancia is round(Dist2).
% test: distancia("castelo queijo","Sra da Luz foz",D).

% Predicado da formula de Haversine em Km, Devolve a Distancia em recta
% https://en.wikipedia.org/wiki/Haversine_formula
% Usada uma implementacao optimizada e TESTADA. Ver fonte.
% p = 0.017453292519943295;    // Math.PI / 180
% 12742 // 2 * R; R = 6371 km Raio da terra.
% https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
% https://stackoverflow.com/questions/43018733/prolog-calculating-distance-between-two-coordinates/43019630
distance(Lat1, Lon1, Lat2, Lon2, Distancia):-
    P is 0.017453292519943295,
    A is (0.5 - cos((Lat2 - Lat1) * P) / 2 + cos(Lat1 * P) * cos(Lat2 * P) * (1 - cos((Lon2 - Lon1) * P)) / 2),
    Distancia is (12742 * asin(sqrt(A))).


% tempo que demora a percorrer a Distancia indicada a velocidade da BC
% Devolve o Tempo.
tempoPercorrerDistancia(Distancia,Tempo):-
    velocidade(V),
    Tempo is Distancia/V.
% test: tempoPercorrerDistancia(75000,T). O tempo deve ser 1.5 horas.




% Obtem qual o no mais perto de um no Origem de uma Lista de Nos.
% Devolve a No Mais Perto
noMaisPerto(Origem, ListaNos, NoMaisPerto):-
    perto(Origem,ListaNos,NoMaisPerto,_).

perto(Origem,[UnicoNo|[]],UnicoNo,Distancia):- distancia(Origem,UnicoNo,Distancia).
perto(Origem,[H|T],Resposta,Distancia):-
    perto(Origem,T,Resposta2,Distancia2),
    distancia(Origem,H,DistanciaOrigemH),
    (   (   (DistanciaOrigemH > Distancia2),Distancia is Distancia2,Resposta = Resposta2);((Distancia is DistanciaOrigemH), Resposta = H)).

% teste: farmacias(L),noMaisPerto("foz leca leixoes",L,R).
% resultado esperado: R = farmacia e falcao.




% Pesquisa Greedy. Dado uma Origem e uma Lista de Farmacias obtem um percurso greedy.
% Devolve a ordem das farmacias da lista de percurso greedy apartir da Origem
% NAO INCLUI A ORIGEM!!!!
greedy(_,[],[]).          % Se a lista de farmacias for vazia a ordem vai vazia
greedy(Origem,ListaFarmacias,OrdemFarmacias):-
    noMaisPerto(Origem,ListaFarmacias,NoMaisPerto),
    delete(ListaFarmacias,NoMaisPerto,ListaFarmacias2),
    greedy(NoMaisPerto,ListaFarmacias2,Ordem),
    OrdemFarmacias = [NoMaisPerto|Ordem].

% teste: greedy("foz leca leixoes",["farmacia e falcao","farmacia Alves de Oliveira","farmacia gramacho","farmacia da foz"],Lresposta).
% resultado esperado: Lresposta = [farmacia e falcao,farmacia gramacho,farmacia da foz,farmacia Alves de Oliveira] .




% Obtem o ultimo elemento de uma lista recebendo um valor Default
% caso lista tiver zero elementos devolve o valor Default.
% Devolve o UltimoElemento.
obterUltimoElementoDeLista(Default,Lista,UltimoElemento):-
    length(Lista,N),
    ( N =:= 0 -> UltimoElemento = Default ; nth1(N,Lista,UltimoElemento)).
% test:  obterUltimoElementoDeLista(1,[1,2,4,3,4,21],R). R = 21.
%        obterUltimoElementoDeLista(12,[],R).      R = 12.



% Obtem qual a rota para as entregas. Metodo de entrada.
% Ver explicacao de como funciona abaixo.
% ListaFarmaciasManha - Lista de farmacias que so recebem de Manha
% ListaFarmaciasManhaTarde - Lista de farmacias que recebem de Manha e Tarde
% ListaFarmaciasTarde - Lista de farmacias que so recebem de Tarde
% Devolve: Resposta - Rota de Nos a passar.
rota(ListaFarmaciasManha,ListaFarmaciasManhaTarde,ListaFarmaciasTarde,Resposta):-
    armazem(Armazem),
    greedy(Armazem,ListaFarmaciasManha,ListaManha),
    obterUltimoElementoDeLista(Armazem,ListaManha,UltimoManha),
    greedy(UltimoManha,ListaFarmaciasManhaTarde,ListaManhaTarde),
    obterUltimoElementoDeLista(UltimoManha,ListaManhaTarde,UltimoManhaTarde),
    greedy(UltimoManhaTarde,ListaFarmaciasTarde,ListaTarde),
    append([Armazem],ListaManha,ListaM),
    append(ListaM,ListaManhaTarde,ListaMMT),
    append(ListaMMT,ListaTarde,ListaMMTT),
    append(ListaMMTT,[Armazem],ListaGreedy),
%    Resposta = ListaGreedy. % provisorio.
processarOrdemFarmacias(ListaGreedy,ListaFarmaciasManha,ListaFarmaciasManhaTarde,ListaFarmaciasTarde,HoraFinal,Rota,Falhas,DistanciaTotal),

    %Debug:
%    write("Hora final = "),write(HoraFinal),nl,
%    write("Falhas = "),write(Falhas),nl,
%    write("Distancia = "),write(DistanciaTotal),nl,
%    write("Lista Inicial = "),write(ListaGreedy),nl,
%    write("Rota inicial = "),write(Rota),nl,

    NiTemp is 20,
    Alfa is 0.95,
    Niteracao is 100,
    Lold = ListaGreedy,
    Dold is DistanciaTotal,
    RM = ListaFarmaciasManha,
    RMT = ListaFarmaciasManhaTarde,
    RT = ListaFarmaciasTarde,
    LbestOld = Lold,
    DbestOld is Dold,
    T is 1,

    cicloDescerTemperatura(NiTemp,Alfa,Niteracao,Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,_,Dout,Lbest,Dbest),

    %Debug output
%    write("Distancia Anel = "),write(Dout),nl,
%    write("Distancia Best = "),write(Dbest),nl,

    processarOrdemFarmacias(Lbest,ListaFarmaciasManha,ListaFarmaciasManhaTarde,ListaFarmaciasTarde,OHoraFinal,ORota,OFalhas,ODistanciaTotal),

    %Debug output
%    write("Hora Optimizada final = "),write(OHoraFinal),nl,
%    write("Falhas Optimizada = "),write(OFalhas),nl,
%    write("Distancia Optimizada = "),write(ODistanciaTotal),nl,
%    write("Lista Optimizada = "),write(Lbest),nl,
    Resposta = [Lbest,ODistanciaTotal,OFalhas,ORota].


% teste: ?- rota(["farmacia matosinhos sul","farmacia Alves de Oliveira","farmacia gramacho"],[],["farmacia fonte luminosa","farmacia benisa","farmacia ferro"],Rota).
% Rota = [rotunda boavista,farmacia Alves de Oliveira,farmacia matosinhos sul,farmacia gramacho,farmacia ferro,farmacia benisa,farmacia fonte luminosa,rotunda boavista] .

% rota(["farmacia Alves de Oliveira","farmacia Antiga da porta do Olival","farmacia benisa","farmacia Coimbroes","farmacia Couto","farmacia da foz"],["farmacia da Prelada","farmacia do campo","farmacia do Passeio Alegre","farmacia dos Clerigos","farmacia dos Montes Burgos","farmacia e falcao"],["farmacia ferreira da silva nortshoping","farmacia ferro","farmacia fonte luminosa","farmacia gramacho","farmacia Lemos"],Rota).
%


% EXPLICACAO do predicao Rota.
% Recebemos 3 listas com as farmacias que so aceitam entregas
% de Manha, de Manha e Tarde, e de Tarde.
% 1) Fazemos o metodo greedy primeiro para as de manha, depois para
% as manhaTarde, e depois para de Tarde.
% Assim garantimos que tentamos comprir as restricoes.
% Obtemos uma lista greedy de ordem de farmacias.
% processamos essa lista, de forma a obter uma rota com detalhe
% do tempo e distancia total correcta.
% 2) Fazemos o simulated anneling com o gready como solucao
% inicial. Durante o simulated anneling, guardamos sempre a melhor
% solucao (lista de armazem - farmacias - armazem).
% 3) Devolvemos como Resposta a melhor solucao e nao a solucao final
% ,a distancia percorida, o numero de falhas, e uma lista com a
% hora as farmacia e a que hora chegamos e qual o caminho.







% Branch and Bound: DEMASIADO LENTO, DEMORA 5 minutos Le�a a Gaia
% Nao usado.

bnb(Orig,Dest,Cam,Custo):- bnb2(Dest,[(0,[Orig])],Cam,Custo).
%condicao final: destino = n� � cabe�a do caminho actual

bnb2(Dest,[(Custo,[Dest|T])|_],Cam,Custo):-
%caminho actual est� invertido
reverse([Dest|T],Cam).

bnb2(Dest,[(Ca,LA)|Outros],Cam,Custo):-
LA=[Act|_],
%calcular todos os nodos adjacentes nao visitados e
%gerar tuplos c/ um caminho novo juntado o nodo + caminho actual
% o custo de cada caminho � o custo acumulado + peso do ramo
findall((CaX,[X|LA]),
(Dest\==Act,ligacao(Act,X,CustoX),\+ member(X,LA),CaX is CustoX + Ca),Novos),
%os novos caminhos sao adicionados aos caminhos n�o explorados
append(Outros,Novos,Todos),
%a ordena��o (n�o sendo o mais eficiente) garante que
% o melhor caminho fica na primeira posi��o
sort(Todos,TodosOrd),
%chamada recursiva
bnb2(Dest,TodosOrd,Cam,Custo).

% teste: bnb("farmacia gramacho","farmacia matosinhos sul",Caminho,Custo).
%  Caminho = [farmacia gramacho,leca igreija,leca polana,ponte Leca
% norte,ponte Leca sul,matosinhos foz leixoes,matosinhos praia av
% republica,matosinhos praia aroso,matosinhos aroso,farmacia matosinhos
% sul] ,
% Custo = 2644 .


% A-Star  + Rapido vamos usar.

aStar(Orig,Dest,Cam,Custo):- aStar2(Dest,[(_,0,
[Orig])],Cam,Custo).
aStar2(Dest,[(_,Custo,[Dest|T])|_],Cam,Custo):-
reverse([Dest|T],Cam).
aStar2(Dest,[(_,Ca,LA)|Outros],Cam,Custo):-
LA=[Act|_],
findall((CEX,CaX,[X|LA]),
(Dest\==Act,ligacao(Act,X,CustoX),\+ member(X,LA),
CaX is CustoX + Ca, distancia(X,Dest,EstX),
CEX is CaX +EstX),Novos),
append(Outros,Novos,Todos),
sort(Todos,TodosOrd),
aStar2(Dest,TodosOrd,Cam,Custo).

% testes:
% Caminho = [rotunda boavista,porto Palacio de Cristal,porto Santo Antonio West,porto Santo Antonio East,porto Rua Carmo West,porto Rua Carmo East,porto Reitoria,porto Clerigos East,porto Aliados Sul,porto Sao Nicolau,ponte Dao Luis Norte,ponte Dao Luis Sul,vci Av Republica Gaia,vci Gerevide Gaia,farmacia Alves de Oliveira],
% Custo = 6537
% ?- aStar("farmacia gramacho","farmacia Alves de Oliveira",Caminho,Custo).Caminho = [farmacia gramacho,leca igreija,leca polana,ponte Leca norte,ponte Leca sul,matosinhos foz leixoes,matosinhos praia av republica,matosinhos praia aroso,rotunda Anemona matosinhos,castelo queijo,Av Boavista com Gomes da Costa,vci boavista,ponte arrabida norte,porto Sao Nicolau,ponte Dao Luis Norte,ponte Dao Luis Sul,vci Av Republica Gaia,vci Gerevide Gaia,farmacia Alves de Oliveira],
% Custo = 14157



% Processa uma sequencia de Armazem - Farmacias - Armazem de forma
% a obter uma rota e verificar se cumpre as restricoes temporais.
% Entadas:
% OrdemFarmacias - lista de [Armazem, Farmacia1, Farmacia2, Armazem].
% Restricoes - Lista de listas [ ListaManha, ListaManhaTarde, ListaTarde].
% Saidas:
% Rota - rota escolhida. Lista de Listas do tipo:
% [[Farmacia,Hora,DistCaminho1,Caminho1],[Farmacia2,Hora,DistCaminho2,Caminho2]]
% Falhas - numero de restricoes quebradas Distancia - distanciaTotal
processarOrdemFarmacias(OrdemFarmacias,RM,RMT,RT,HoraFinal,Rota,Falhas,DistanciaTotal):-
    OrdemFarmacias = [Armazem|OrdemSeguinte],
    horaInicio(HoraInicio),
    processarOrdemAux(Armazem,OrdemSeguinte,RM,RMT,RT,HoraInicio,[],0,0,HoraFinal,Rota,Falhas,DistanciaTotal).


processarOrdemAux(_,[],_,_,_,HoraFinal,Rota,Falhas,DistanciaTotal,HoraFinal,Rota,Falhas,DistanciaTotal).
processarOrdemAux(Origem,[Farmacia|OrdemSeguinte],RM,RMT,RT,HoraTras,RotaTras,FalhasTras,DistanciaTras,HoraFinal,Rota,Falhas,DistanciaTotal):-
    aStar(Origem,Farmacia,Caminho,Dist),
    tempoPercorrerDistancia(Dist,Tempo),
    HoraAgora is HoraTras + Tempo,
    DistAgora is DistanciaTras + Dist,
    processarTempo(Farmacia,HoraAgora,RM,RMT,RT,HoraAgoraCorrigida,Falha),
    FalhasAgora is FalhasTras + Falha,
    append(RotaTras,[Farmacia,HoraAgora,Dist,Caminho],RotaAgora),
    processarOrdemAux(Farmacia,OrdemSeguinte,RM,RMT,RT,HoraAgoraCorrigida,RotaAgora,FalhasAgora,DistAgora,HoraFinal,Rota,Falhas,DistanciaTotal).







% Predicado que verifica a hora da entrega na farmacia.
% Se for demasiado cedo, espera avancando a hora
% Se for demasiado % tarde cria uma falha.
% Farmacia: farmacia que estamos a verificar
% Hora: hora a que estimanos chegar.
% RM: Restricao da Manha, Lista de Farmacias de Manha
% RMT: Restricao de ManhaTarde, Lista de Farmacias de Manha e Tarde
% RT: RM: Restricao da Tarde, Lista de Farmacias de Tarde
% HoraCorrigida: Hora correcta apos possivel correcao
% Falha: 0 se nao existiu uma falha, 1 se existiu uma falha.
processarTempo(Farmacia,Hora,RM,_,RT,HoraCorigida,Falha):-
    ( member(Farmacia,RM) -> verificarManha(Hora,CorrecaoHM,FalhaManha) ; verificarNada(Hora,CorrecaoHM,FalhaManha)),
    ( member(Farmacia,RT) -> verificarTarde(Hora,CorrecaoHT,FalhaTarde) ; verificarNada(Hora,CorrecaoHT,FalhaTarde)),
    HoraCorigida is Hora + CorrecaoHM + CorrecaoHT,
    Falha is FalhaManha + FalhaTarde.

% testes:
% processarTempo("farmacia gramacho",10,["farmacia matosinhos sul","farmacia Alves de Oliveira","farmacia gramacho"],[],["farmacia fonte luminosa","farmacia benisa","farmacia ferro"],HoraC,Falha).
% Resultado: HoraC = 10, Falha = 0
% ?- processarTempo("farmacia gramacho",15,["farmacia matosinhos sul","farmacia Alves de Oliveira","farmacia gramacho"],[],["farmacia fonte luminosa","farmacia benisa","farmacia ferro"],HoraC,Falha).
% Resultado: HoraC = 15,Falha = 1
% ?- processarTempo("farmacia gramacho",15,["farmacia matosinhos sul","farmacia Alves de Oliveira"],["farmacia gramacho"],["farmacia fonte luminosa","farmacia benisa","farmacia ferro"],HoraC,Falha).
% HoraC = 15, Falha = 0.
% ?- processarTempo("farmacia gramacho",15,["farmacia matosinhos sul","farmacia Alves de Oliveira"],[],["farmacia fonte luminosa","farmacia gramacho","farmacia benisa","farmacia ferro"],HoraC,Falha).
% HoraC = 15, Falha = 0.
% ?- processarTempo("farmacia gramacho",10,["farmacia matosinhos sul","farmacia Alves de Oliveira"],[],["farmacia fonte luminosa","farmacia gramacho","farmacia benisa","farmacia ferro"],HoraC,Falha).
% HoraC = 13, Falha = 0

%Predicados auxiliares de processarTempo/7.
verificarNada(_,0,0).
verificarManha(Hora,0,FalhaManha):-
    horaTarde(HoraTarde),
    (   Hora > HoraTarde ->  FalhaManha is 1 ; FalhaManha is 0).
verificarTarde(Hora,CorrecaoHT,0):-
    horaTarde(HoraTarde),
    (   Hora < HoraTarde,  CorrecaoHT is HoraTarde - Hora ; CorrecaoHT is 0).

% testes:
% ?- verificarManha(15,HCM,FM). HCM = 0, FM = 1.
% ?- verificarTarde(15,HCM,FM). HCM = FM, FM = 0.
% ?- verificarTarde(11,HCM,FM). HCM = 2, FM = 0 .
% ?- verificarManha(10,HCM,FM). HCM = FM, FM = 0.




% #############################################
% #         SIMULATED ANNEALING               #
% #############################################


% Cria uma nova solucao, trocando dois elementos da lista
% sem nunca trocar o primeiro ou o ultimo.
% SolucaoAnterior: Lista de elementos
% Devolve  NovaSolucao
novaSolucao(SolucaoAnterior,NovaSolucao):-
    length(SolucaoAnterior,Nelementos),
    N is Nelementos-1,
    % N-1 evita o ultimo elemento, e 2 evita o primeiro
    random_between(2,N,Index1), % write(Pos1),nl,
    random_between(2,N,Index2), % write(Pos2),nl,
    nth1(Index1,SolucaoAnterior,Elemento1),
    nth1(Index2,SolucaoAnterior,Elemento2),
    substituirElemento(Elemento2,SolucaoAnterior,Index1,LAux),
    substituirElemento(Elemento1,LAux,Index2,NovaSolucao).

% Teste: O resultado e aleatorio, raramente da o mesmo resultado.
% novaSolucao([1,2,3,4,5,6,7,8,9],R). R = [1, 7, 3, 4, 5, 6, 2, 8, 9] .



% Predicado que substitui o elemento de index Posicao1 por NovoElemento
% em uma Lista. Usado no obter uma nova solucao.
% A contagem da posicao dos elementos comeca em 1 e nao zero.
% [1,2,3,4,5,6,....
% Devolve a nova Lista.
substituirElemento(NovoElemento,Lista,Posicao1,Resultado):-
    substituirAux(NovoElemento,Posicao1,[],Lista,Resultado).

substituirAux(_,_,Frente,[],Frente).
substituirAux(Elemento,1,Frente,Tras,Res):-
    Tras = [_|T],
    append(Frente,[Elemento],FrenteH),
    append(FrenteH,T,Res).

substituirAux(Elemento,Pos1,Frente,Tras,Res):-
    PosNext is Pos1-1,
    Tras = [H|T],
    append(Frente,[H],FrenteH),
    substituirAux(Elemento,PosNext,FrenteH,T,Res).

% Teste:
% substituirElemento(a,[1,2,3,4,5,6,7],5,R). R = [1, 2, 3, 4, a, 6, 7] .





% Faz uma iteracao do simulated annealing a uma temperatura constante.
% Lold/Dold: solucao e distancia antiga
% Lnova/Dnova: solucao e distancia nova
% RM/RMT/RT: restricoes temporais, farmacias apenas Manha, ManhaTarde e Tarde
% LbestOld/DbestOld: melhor solucao antiga e susua distancia
% T: Temperatura
% Lout/Dout: Resposta, solucao que prosegue para a proxima iteracao.
%
iteracaoAnel(Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,Lout,Dout,Lbest,Dbest):-
    novaSolucao(Lold,Lnova),
    processarOrdemFarmacias(Lnova,RM,RMT,RT,_,_,Falhas,Dnova),
    (   Falhas =:= 0 ->
    iteracaoAnelCompare(Lold,Dold,Lnova,Dnova,LbestOld,DbestOld,T,Lout,Dout,Lbest,Dbest)
    ; iteracaoAnelNada(Lold,Dold,LbestOld,DbestOld,Lout,Dout,Lbest,Dbest) ).

% Teste: Funciona, mas o idea � testar fora.
% ?- iteracaoAnel(["rotunda boavista","farmacia Antiga da porta do Olival","farmacia Couto","farmacia Alves de Oliveira","farmacia Coimbroes","farmacia da foz","farmacia benisa","farmacia e falcao","farmacia do Passeio Alegre","farmacia da Prelada","farmacia dos Montes Burgos","farmacia do campo","farmacia dos Clerigos","farmacia Lemos","farmacia ferreira da silva nortshoping","farmacia fonte luminosa","farmacia gramacho","farmacia ferro","rotunda boavista"],76917,["farmacia Alves de Oliveira","farmacia Antiga da porta do Olival","farmacia benisa","farmacia Coimbroes","farmacia Couto","farmacia da foz"],["farmacia da Prelada","farmacia do campo","farmacia do Passeio Alegre","farmacia dos Clerigos","farmacia dos Montes Burgos","farmacia e falcao"],["farmacia ferreira da silva nortshoping","farmacia ferro","farmacia fonte luminosa","farmacia gramacho","farmacia Lemos"],[1,2],85000,1,Lout,Dout,Lbest,Dbest).
% Lout = Lbest, Lbest = ["rotunda boavista", "farmacia Antiga da porta do Olival", "farmacia Couto", "farmacia Alves de Oliveira", "farmacia Coimbroes", "farmacia da foz", "farmacia benisa", "farmacia e falcao",
% "farmacia ferro"|...], Dout = Dbest, Dbest = 72146 .



% Se ocorreu uma falha ignoramos esta nova solucao.
iteracaoAnelNada(Lold,Dold,LbestOld,DbestOld,Lold,Dold,LbestOld,DbestOld).

% Nao tendo ocorrido uma falha, vamos aplicar o annealing ficando com a melhor.
iteracaoAnelCompare(Lold,Dold,Lnova,Dnova,LbestOld,DbestOld,T,Lout,Dout,Lbest,Dbest):-
    (   Dnova < DbestOld ->
      (   Dbest is Dnova , Lbest = Lnova); (Dbest is DbestOld, Lbest = LbestOld)
    ) ,
    (   Dnova < Dold ->
         (   Dout is Dnova, Lout = Lnova);
         verificarAceitacaoAnel(Lold,Dold,Lnova,Dnova,T,Lout,Dout)
    ).

% Testes:
% Teste1, para uma nova solucao pior, consegue alternar entre as 2.
% ?- iteracaoAnelCompare([1,2,3],75000,[4,5,6],76000,[7,8,9],75000,1,Lout,Dout,Lbest,Dbest).
% Lout = [1, 2, 3], Dout = Dbest, Dbest = 75000, Lbest = [7, 8, 9].
% ?- iteracaoAnelCompare([1,2,3],75000,[4,5,6],76000,[7,8,9],75000,1,Lout,Dout,Lbest,Dbest).
% Lout = [4, 5, 6], Dout = 76000, Lbest = [7, 8, 9], Dbest = 75000.
%
% Teste2, para uma solucao melhor, aceita sempre essa e corrige o best.
% ?- iteracaoAnelCompare([1,2,3],75000,[4,5,6],74000,[7,8,9],75000,1,Lout,Dout,Lbest,Dbest).
% Lout = Lbest, Lbest = [4, 5, 6], Dout = Dbest, Dbest = 74000.




% Verifica para um solucao pior, qual a que deve proseguir.
% Lold/Dold: solucao e distancia antiga
% Lnova/Dnova: solucao e distancia nova
% T: Temperatura
% Lout/Dout: Resposta, solucao que prosegue para a proxima iteracao.
%
verificarAceitacaoAnel(Lold,Dold,Lnova,Dnova,T,Lout,Dout):-
    acceptance_Probability(Dold,Dnova,T,R),
    random(Rand),
    (    Rand < R ->
       (   Lout = Lnova, Dout is Dnova);
       (   Lout = Lold,  Dout is Dold)
    ).
% Teste: O resultado tanto pode ser um como o outro. Aleatorio.
% ?- verificarAceitacaoAnel([1,2,3],75000,[4,5,6],76000,1,Lout,Dout).
% Lout = [1, 2, 3], Dout = 75000.
% ?- verificarAceitacaoAnel([1,2,3],75000,[4,5,6],76000,1,Lout,Dout).
% Lout = [4, 5, 6], Dout = 76000.
% teste de valor exagerado:
% ?- verificarAceitacaoAnel([1,2,3],75000,[4,5,6],99000,1,Lout,Dout).
% Lout = [1, 2, 3], Dout = 75000.


% Calcula a probabilidade de aceitacao R, com base na
% Dold: Distancia da solucao anterior
% Dnova: Distancia da solucao nova
acceptance_Probability(Dold,Dnova,T,R):-
    Diferenca is ((Dold - Dnova)/1000),     % passar a Km.
     (   Diferenca <  -10 -> R is 0 ;       % limitar para nao extourar.
      R is e^(Diferenca/T)).

% Teste:
% ?- acceptance_Probability(75000,76000,1,R). R = 0.36787944117144233.
% ?- acceptance_Probability(75000,86000,1,R). R = 0.


% Corre N iteracoes a Temperatura constante.
% Niteracao: numero de iteracoes a correr (vai decrementando)%
% Lold/Dold: solucao e distancia antiga
% Lnova/Dnova: solucao e distancia nova
% RM/RMT/RT: restricoes temporais, farmacias apenas Manha, ManhaTarde e Tarde
% LbestOld/DbestOld: melhor solucao antiga e susua distancia
% T: Temperatura
% Lout/Dout: Resposta, solucao que prosegue para a proxima iteracao.
%
cicloIterarAnel(0,Lold,Dold,_,_,_,LbestOld,DbestOld,_,Lold,Dold,LbestOld,DbestOld).
cicloIterarAnel(Niteracao,Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,Lout,Dout,Lbest,Dbest):-
    iteracaoAnel(Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,LoutAnterior,DoutAnterior,LbestAnterior,DbestAnterior),
    N is Niteracao -1,
    cicloIterarAnel(N,LoutAnterior,DoutAnterior,RM,RMT,RT,LbestAnterior,DbestAnterior,T,Lout,Dout,Lbest,Dbest).

% teste: Funciona
% cicloIterarAnel(100,["rotunda boavista","farmacia Antiga da porta do Olival","farmacia Couto","farmacia Alves de Oliveira","farmacia Coimbroes","farmacia da foz","farmacia benisa","farmacia e falcao","farmacia do Passeio Alegre","farmacia da Prelada","farmacia dos Montes Burgos","farmacia do campo","farmacia dos Clerigos","farmacia Lemos","farmacia ferreira da silva nortshoping","farmacia fonte luminosa","farmacia gramacho","farmacia ferro","rotunda boavista"],76917,["farmacia Alves de Oliveira","farmacia Antiga da porta do Olival","farmacia benisa","farmacia Coimbroes","farmacia Couto","farmacia da foz"],["farmacia da Prelada","farmacia do campo","farmacia do Passeio Alegre","farmacia dos Clerigos","farmacia dos Montes Burgos","farmacia e falcao"],["farmacia ferreira da silva nortshoping","farmacia ferro","farmacia fonte luminosa","farmacia gramacho","farmacia Lemos"],[1,2],85000,1,Lout,Dout,Lbest,Dbest).
% Lout = Lbest, Lbest = ["rotunda boavista", "farmacia Coimbroes",
% "farmacia Couto", "farmacia Alves de Oliveira", "farmacia dos Montes Burgos", "farmacia da Prelada", "farmacia benisa", "farmacia e falcao", "farmacia do Passeio Alegre"|...],
% Dout = Dbest, Dbest = 74747 .
% Teste unitario ideal, sera confirmar que temos um valor de Dout/Dbest
% melhor que o da lista inicial de 76917.


% Predicado onde fazemos o ciclo onde desce a temperatura, e se chama as
%    iteracoes a temperatura constante
% NiTemp: numero de vezes que desce a temperatura, ou seja o
%   numero de iteracacoes do ciclo da temperatura, recomendado 20
% Alfa: decaimento da temperatura, 0.8 a 0.99 usar 0.95
% Niteracao: numero de iteracoes a correr (vai decrementando)%
% Lold/Dold: solucao e distancia antiga
% Lnova/Dnova: solucao e distancia nova
% RM/RMT/RT: restricoes temporais, farmacias apenas Manha, ManhaTarde e Tarde
% LbestOld/DbestOld: melhor solucao antiga e susua distancia
% T: Temperatura
% Lout/Dout: Resposta, solucao que prosegue para a proxima iteracao.
%
cicloDescerTemperatura(0,_,_,Lold,Dold,_,_,_,LbestOld,DbestOld,_,Lold,Dold,LbestOld,DbestOld).
cicloDescerTemperatura(NiTemp,Alfa,Niteracao,Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,Lout,Dout,Lbest,Dbest):-
    cicloIterarAnel(Niteracao,Lold,Dold,RM,RMT,RT,LbestOld,DbestOld,T,LoutAnterior,DoutAnterior,LbestAnterior,DbestAnterior),
    !,
    Tnova is T * Alfa,  %write("T: "), write(Tnova),nl,
    NiTempNext is NiTemp-1,
    cicloDescerTemperatura(NiTempNext,Alfa,Niteracao,LoutAnterior,DoutAnterior,RM,RMT,RT,LbestAnterior,DbestAnterior,Tnova,Lout,Dout,Lbest,Dbest).

% Teste:
% ?- cicloDescerTemperatura(20,0.95,100,["rotunda boavista","farmacia Antiga da porta do Olival","farmacia Couto","farmacia Alves de Oliveira","farmacia Coimbroes","farmacia da foz","farmacia benisa","farmacia e falcao","farmacia do Passeio Alegre","farmacia da Prelada","farmacia dos Montes Burgos","farmacia do campo","farmacia dos Clerigos","farmacia Lemos","farmacia ferreira da silva nortshoping","farmacia fonte luminosa","farmacia gramacho","farmacia ferro","rotunda boavista"],76917,["farmacia Alves de Oliveira","farmacia Antiga da porta do Olival","farmacia benisa","farmacia Coimbroes","farmacia Couto","farmacia da foz"],["farmacia da Prelada","farmacia do campo","farmacia do Passeio Alegre","farmacia dos Clerigos","farmacia dos Montes Burgos","farmacia e falcao"],["farmacia ferreira da silva nortshoping","farmacia ferro","farmacia fonte luminosa","farmacia gramacho","farmacia Lemos"],[1,2],85000,1,Lout,Dout,Lbest,Dbest).
% T: 0.95
% T: 0.9025
% T: 0.8573749999999999
% T: 0.8145062499999999
% T: 0.7737809374999999
% T: 0.7350918906249998
% T: 0.6983372960937497
% T: 0.6634204312890623
% T: 0.6302494097246091
% T: 0.5987369392383786
% T: 0.5688000922764596
% T: 0.5403600876626365
% T: 0.5133420832795047
% T: 0.48767497911552943
% T: 0.46329123015975293
% T: 0.44012666865176525
% T: 0.41812033521917696
% T: 0.3972143184582181
% T: 0.37735360253530714
% T: 0.35848592240854177
%  Lout = ["rotunda boavista", "farmacia dos Clerigos", "farmacia Alves  de Oliveira", "farmacia Couto", "farmacia Coimbroes", "farmacia do Passeio Alegre", "farmacia da foz", "farmacia e falcao", "farmacia benisa"|...],
% Dout = 70945,
% Lbest = ["rotunda boavista", "farmacia do campo", "farmacia Couto", "farmacia Alves de Oliveira", "farmacia Coimbroes", "farmacia do Passeio Alegre", "farmacia da foz", "farmacia e falcao", "farmacia benisa"|...],
% Dbest = 69338 .
% Teste unitario ideal, sera confirmar que temos um valor de Dout/Dbest
% melhor que o da lista inicial de 76917.


% MANEIRA de mostar a lista completa de resultados.
%set_prolog_flag(answer_write_options,[max_depth(0)]).







