% SERVIDOR PROLOG PARA OBTENCAO DE ROTAS

%Bibliotecas
:- use_module(library(http/http_unix_daemon)).
:-consult('main.pl').
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- initialization http_daemon.

% Relacao entre pedidos HTTP e predicados que os processam
:- http_handler('/lapr5', responde_ola, []).
:- http_handler('/register_user', register_user, []).
:- http_handler('/send_file_post', send_file_post, []).
:- http_handler('/heartbeat', hearthbeat, []).
:- http_handler('/criarrota', criarrota, [time_limit(1800)]).

% Autoporto  - Desactivado, usamo o Daemon.
% :- http_server(http_dispatch, [port(5000),timeout(1800),keep_alive_timeout(1800)]).


% Criacao de servidor HTTP no porto 'Port'
server(Port) :-
        http_server(http_dispatch, [port(Port)]).


criarrota(Request):-
        % OBter os parametros
        http_parameters(Request,
                        [ rm(RM, []),
                          rmt(RMT, []),
                          rt(RT, [])
                        ],
                        [form_data(AsForm)]
                       ),
        %Formatar resposta.
        format('Content-type: text/plain~n~n'),

        %Testes dos objectos de entrada.
%        format('output = ~w ~w ~w ~n',[RM,RMT,RT]),
%        write("AsForm = "),write(AsForm),nl,

        % Transformar entradas em listas
        split_string(RM, ",", "\"[]", LRM),
        split_string(RMT, ",", "\"[]", LRMT),
        split_string(RT, ",", "\"[]", LRT),

        %Testes dos objectos transformados em listas
%        write("LRM = "),write(LRM),nl,
%        write("LRMT = "),write(LRMT),nl,
%        write("LRT = "),write(LRT),nl,
%        LRM = [LL|_],
%        write(LL),nl,
%        farmacias(Farmacias),
%        (   member(LL,Farmacias) -> write("true"); write("false")),nl,nl,



        % Correcao para listas vazias, so funciona assim
        % nao detecta entrada de [] com uma lista vazia
        string_length(RM, LenRM)
        ,string_length(RMT, LenRMT)
        ,string_length(RT, LenRT)

        ,( LenRM =< 4 -> LRM2 = [] ; LRM2 = LRM)
        ,( LenRMT =< 4 -> LRMT2 = [] ; LRMT2 = LRMT)
        ,( LenRT =< 4 -> LRT2 = [] ; LRT2 = LRT)


        %Obtencao das rotas
        ,rota(LRM2, LRMT2, LRT2,Resposta)

        %rota(["farmacia Alves de Oliveira","farmacia Antiga da porta do Olival","farmacia benisa"],[],[],Resposta),


        %Teste check a resposta
%        ,write("Resposta = "),write(Resposta),nl

        %Responder
        ,write(Resposta)
        .





% Teste para verificar se o servidor se encontra online
hearthbeat(_Request):-
              format('Content-type: text/plain~n~n'),
              format('prolog online~n').




% Tratamento de 'http://localhost:5000/lapr5'
responde_ola(_Request) :-
        format('Content-type: text/plain~n~n'),
        format('Ola LAPR5!~n').

% MeTODO GET: Tratamento de 'http://localhost:5000/register_user?name='Jose'&sex=male&birth_year=1975'
% ou http_client:http_get('http://localhost:5000/register_user?name=\'Jose\'&sex=male&birth_year=1975',X,[]).

% MeTODO POST
% http_client:http_post('http://localhost:5000/register_user', form_data([name='Jose', sex=male, birth_year=1975]), Reply, []).

register_user(Request) :-
    http_parameters(Request,
                    [ name(Name, []),
                      sex(Sex, [oneof([male,female])]),
                      birth_year(BY, [between(1850,10000)])
                    ]),
    format('Content-type: text/plain~n~n'),
    format('User registered!~n'),
	format('Name: ~w~nSex: ~w~nBirth Year: ~w~n',[Name,Sex,BY]).

% MeTODO POST enviando um ficheiro de texto
% http_client:http_post('http://localhost:5000/send_file_post', form_data([file=file('./teste.txt')]), Reply, []).

send_file_post(Request) :-
	http_parameters(Request,[ file(X,[])]),
    format('Content-type: text/plain~n~n'),
	format('Received: ~w~n',[X]).





