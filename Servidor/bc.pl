
% nos ou vertices do grafo.
%no(nome, latitude, longitude
no("castelo queijo",41.16785059178,-8.68825443252).
no("Forte da foz",41.14881084921,-8.67443383593).
no("ponte arrabida norte",41.14844180733,-8.64061748257).
no("vci boavista",41.16113565503,-8.64934112156).
no("No Bessa",41.16718650782,-8.64218577722).
no("rotunda boavista",41.15781482765,-8.62885527271).
no("vci curva da prelada",41.17080199463,-8.63679476437).
no("rotunda aep",41.17803236973,-8.65571231857).
no("vci no da prelada",41.17345814352,-8.62238605729).
no("no matosinhos",41.18836638748,-8.6688159485).
no("no Leixoes Sul",41.19205867968,-8.67950275632).
no("no Leixoes norte",41.19635766163,-8.68400600238).
no("ponte Leca norte",41.18988379304,-8.69563378699).
no("ponte Leca sul",41.18791115896,-8.69328134502).
no("rotunda Anemona matosinhos",41.17329158265,-8.68871088633).
no("foz leca leixoes",41.18682363049,-8.70141403937).
no("leca inicio da praia",41.19016195496,-8.70490909602).
no("leca praia marginal",41.19188163144,-8.70568204124).
no("leca praia farol",41.19901274887,-8.71001725573).
no("no exponor",41.20293200383,-8.69055919999).
no("leca igreija",41.19312078208,-8.69680997437).
no("leca polana",41.19112295621,-8.69583539127).
no("leca rotunda amorosa",41.20217348196,-8.69644033852).
no("farmacia benisa",41.20626953217,-8.68729942114).
no("farmacia ferro",41.19661053413,-8.70625338218).
no("farmacia gramacho",41.19443579871,-8.69818786685).
no("farmacia e falcao",41.1894032598,-8.70060748784).
no("matosinhos foz leixoes",41.18528088773,-8.69677640166).
no("matosinhos cirvunvalacao queima",41.17233031971,-8.68057815837).
no("matosinhos fonte luminosa",41.17900825082,-8.68131746366).
no("matosinhos rotunda av republica",41.18060175344,-8.68215762151).
no("matosinhos sul av republica",41.1799947094,-8.69049198736).
no("matosinhos praia av republica",41.17974177272,-8.69368458718).
no("matosinhos torta noz av republica",41.18034881911,-8.6857534971).
no("farmacia peninsular",41.18098115311,-8.68494694557).
no("farmacia moderna",41.18075351357,-8.68975264845).
no("farmacia fonte luminosa",41.17840119201,-8.68007403005).
no("matosinhos camera sul",41.18211933894,-8.6830985983).
no("matosinhos camera norte",41.18378864236,-8.68387154352).
no("matosinhos aroso",41.17660527674,-8.68918134112).
no("matosinhos praia aroso",41.17647880198,-8.69123132626).
no("farmacia matosinhos sul",41.17738941482,-8.68827397064).
no("matosinhos rotunda aroso",41.1772123522,-8.6811494321).
no("matosinhos senhor matosinhos",41.18836636219,-8.68608956024).
no("farmacia parque",41.1851291128,-8.6823592594).
no("farmacia ferreira da silva nortshoping",41.17996944107,-8.65510457243).
no("circunvalacao alto do viso",41.18438299264,-8.64214928802).
no("circunvalacao curva blockbuster",41.17921062781,-8.63225227899).
no("circunvalacao montes burgos",41.17950146932,-8.62959732978).
no("circunvalacao no via norte",41.18048792046,-8.62222074388).
no("circunvalacao amial",41.18325750498,-8.61346631592).
no("circunvalacao Sao Tome",41.18449681884,-8.60950077088).
no("circunvalacao IPO",41.18386451878,-8.60355245332).
no("circunvalacao roberto frias",41.18224580281,-8.59713364736).
no("circunvalacao IP1",41.18095585987,-8.59296646444).
no("farmacia Sao Joao",41.18247343716,-8.60193935025).
no("vci A3",41.17187497971,-8.59541975896).
no("porto Clerigos West",41.14553574044,-8.61554990738).
no("porto Clerigos East",41.14599126346,-8.61403762325).
no("vci no Paranhos",41.17178939681,-8.60443497341).
no("farmacia da Prelada",41.16930168062,-8.63315688215).
no("porto Montes Burgos",41.17590522887,-8.62792925441).
no("farmacia dos Montes Burgos",41.17644795666,-8.62570601043).
no("porto Palacio de Cristal",41.14871786011,-8.62546565973).
no("porto Santo Antonio West",41.14740568254,-8.62053844032).
no("porto Santo Antonio East",41.14808438666,-8.61813493331).
no("porto Rua Carmo West",41.14717944628,-8.61789458261).
no("porto Tribunal",41.14480391836,-8.61729370586).
no("porto Reitoria",41.14722468228,-8.61522066604).
no("porto Rua Carmo East",41.1472925532,-8.61604687157).
no("porto Praca Carlos Alberto",41.14828798521,-8.61589665239).
no("farmacia Lemos",41.14853684085,-8.61540092906).
no("farmacia Antiga da porta do Olival",41.14539214103,-8.61480005231).
no("porto Aliados Sul",41.14607087729,-8.6114952452).
no("porto Aliados Norte",41.15351376665,-8.61005314099).
no("farmacia dos Clerigos",41.14656860446,-8.61302748092).
no("porto Praca da Republica",41.15380784522,-8.61344809464).
no("porto Lapa",41.15527821833,-8.61332791929).
no("farmacia do campo",41.1561378058,-8.61401892756).
no("porto Sao Nicolau",41.14107079468,-8.61446961517).
no("porto Ponte do Freixo Norte",41.14392158013,-8.58082051705).
no("porto Ponte do Freixo Sul",41.13803887101,-8.58058016635).
no("vci Antas",41.16409983222,-8.58088066482).
no("circunvalacao Magalhaes Lemos",41.17875454482,-8.66344113054).
no("ponte arrabida sul",41.14645555285,-8.64024728791).
no("vci Gerevide Gaia",41.12921381798,-8.59349901652).
no("vci Av Republica Gaia",41.12600029872,-8.60551655156).
no("vci No Telheira Gaia",41.12188133265,-8.62029811966).
no("gaia Rotunda Engenheiro Edgar Cardoso",41.13048107807,-8.62606653648).
no("vci no Gandara Gaia",41.1153176108,-8.62636697485).
no("vci no Coimbroes Gaia",41.12772023001,-8.63568056451).
no("farmacia Alves de Oliveira",41.12817288128,-8.58953329004).
no("farmacia Couto",41.12640765495,-8.60695865577).
no("farmacia Coimbroes",41.12781075146,-8.63333714518).
no("vci ArrabidaShoping West gaia",41.13885343143,-8.63676214266).
no("vci ArrabidaShoping East gaia",41.13998474858,-8.63381784658).
no("ponte Dao Luis Norte",41.14084453656,-8.60960251347).
no("ponte Dao Luis Sul",41.13921545504,-8.60918189975).
no("passeio Alegre porto",41.14738305895,-8.66665573103).
no("farmacia do Passeio Alegre",41.14903458323,-8.66626519119).
no("Praca do Imperio porto",41.1552329992,-8.67269457243).
no("Av Boavista com Gomes da Costa",41.16224509896,-8.65550949733).
no("Sra da Luz foz",41.15224697415,-8.67756167413).
no("farmacia da foz",41.15211124251,-8.67569895619).

% ligacoes no1 para no2 com distancia em metros, e no2 para no1 com a mesma distancia Arestas
%ligacao(no1 , no2, distance)
ligacao("vci boavista","No Bessa",901).
ligacao("No Bessa","vci boavista",901).
ligacao("rotunda boavista","vci boavista",1754).
ligacao("vci boavista","rotunda boavista",1754).
ligacao("ponte arrabida norte","rotunda boavista",1434).
ligacao("rotunda boavista","ponte arrabida norte",1434).
ligacao("vci curva da prelada","No Bessa",604).
ligacao("No Bessa","vci curva da prelada",604).
ligacao("rotunda aep","No Bessa",1654).
ligacao("No Bessa","rotunda aep",1654).
ligacao("vci no da prelada","vci curva da prelada",1242).
ligacao("vci curva da prelada","vci no da prelada",1242).
ligacao("vci boavista","ponte arrabida norte",1589).
ligacao("ponte arrabida norte","vci boavista",1589).
ligacao("no matosinhos","rotunda aep",1588).
ligacao("rotunda aep","no matosinhos",1588).
ligacao("no Leixoes Sul","no matosinhos",984).
ligacao("no matosinhos","no Leixoes Sul",984).
ligacao("no Leixoes norte","no Leixoes Sul",609).
ligacao("no Leixoes Sul","no Leixoes norte",609).
ligacao("ponte Leca norte","no Leixoes norte",1210).
ligacao("no Leixoes norte","ponte Leca norte",1210).
ligacao("ponte Leca sul","ponte Leca norte",295).
ligacao("ponte Leca norte","ponte Leca sul",295).
ligacao("rotunda Anemona matosinhos","castelo queijo",606).
ligacao("castelo queijo","rotunda Anemona matosinhos",606).
ligacao("foz leca leixoes","ponte Leca norte",591).
ligacao("ponte Leca norte","foz leca leixoes",591).
ligacao("leca inicio da praia","foz leca leixoes",473).
ligacao("foz leca leixoes","leca inicio da praia",473).
ligacao("leca praia marginal","leca inicio da praia",202).
ligacao("leca inicio da praia","leca praia marginal",202).
ligacao("leca praia farol","leca praia marginal",872).
ligacao("leca praia marginal","leca praia farol",872).
ligacao("no exponor","no Leixoes norte",914).
ligacao("no Leixoes norte","no exponor",914).
ligacao("leca igreija","leca polana",237).
ligacao("leca polana","leca igreija",237).
ligacao("leca polana","ponte Leca norte",139).
ligacao("ponte Leca norte","leca polana",139).
ligacao("leca igreija","leca praia marginal",755).
ligacao("leca praia marginal","leca igreija",755).
ligacao("leca rotunda amorosa","no exponor",499).
ligacao("no exponor","leca rotunda amorosa",499).
ligacao("leca rotunda amorosa","leca praia farol",1189).
ligacao("leca praia farol","leca rotunda amorosa",1189).
ligacao("farmacia benisa","no exponor",461).
ligacao("no exponor","farmacia benisa",461).
ligacao("farmacia ferro","leca praia farol",413).
ligacao("leca praia farol","farmacia ferro",413).
ligacao("farmacia gramacho","leca igreija",186).
ligacao("leca igreija","farmacia gramacho",186).
ligacao("farmacia e falcao","leca inicio da praia",370).
ligacao("leca inicio da praia","farmacia e falcao",370).
ligacao("leca rotunda amorosa","leca igreija",1007).
ligacao("leca igreija","leca rotunda amorosa",1007).
ligacao("matosinhos foz leixoes","ponte Leca sul",414).
ligacao("ponte Leca sul","matosinhos foz leixoes",414).
ligacao("matosinhos cirvunvalacao queima","rotunda Anemona matosinhos",689).
ligacao("rotunda Anemona matosinhos","matosinhos cirvunvalacao queima",689).
ligacao("matosinhos rotunda av republica","matosinhos fonte luminosa",191).
ligacao("matosinhos fonte luminosa","matosinhos rotunda av republica",191).
ligacao("matosinhos praia av republica","matosinhos sul av republica",269).
ligacao("matosinhos sul av republica","matosinhos praia av republica",269).
ligacao("matosinhos torta noz av republica","matosinhos rotunda av republica",302).
ligacao("matosinhos rotunda av republica","matosinhos torta noz av republica",302).
ligacao("matosinhos torta noz av republica","matosinhos sul av republica",399).
ligacao("matosinhos sul av republica","matosinhos torta noz av republica",399).
ligacao("farmacia peninsular","matosinhos torta noz av republica",97).
ligacao("matosinhos torta noz av republica","farmacia peninsular",97).
ligacao("farmacia moderna","matosinhos sul av republica",105).
ligacao("matosinhos sul av republica","farmacia moderna",105).
ligacao("farmacia fonte luminosa","matosinhos fonte luminosa",124).
ligacao("matosinhos fonte luminosa","farmacia fonte luminosa",124).
ligacao("matosinhos camera sul","matosinhos rotunda av republica",186).
ligacao("matosinhos rotunda av republica","matosinhos camera sul",186).
ligacao("matosinhos camera norte","matosinhos camera sul",197).
ligacao("matosinhos camera sul","matosinhos camera norte",197).
ligacao("matosinhos aroso","rotunda Anemona matosinhos",371).
ligacao("rotunda Anemona matosinhos","matosinhos aroso",371).
ligacao("matosinhos aroso","matosinhos sul av republica",393).
ligacao("matosinhos sul av republica","matosinhos aroso",393).
ligacao("matosinhos praia aroso","matosinhos praia av republica",417).
ligacao("matosinhos praia av republica","matosinhos praia aroso",417).
ligacao("matosinhos praia aroso","rotunda Anemona matosinhos",412).
ligacao("rotunda Anemona matosinhos","matosinhos praia aroso",412).
ligacao("matosinhos praia aroso","matosinhos aroso",172).
ligacao("matosinhos aroso","matosinhos praia aroso",172).
ligacao("farmacia matosinhos sul","matosinhos aroso",116).
ligacao("matosinhos aroso","farmacia matosinhos sul",116).
ligacao("matosinhos rotunda aroso","matosinhos fonte luminosa",200).
ligacao("matosinhos fonte luminosa","matosinhos rotunda aroso",200).
ligacao("matosinhos aroso","matosinhos rotunda aroso",676).
ligacao("matosinhos rotunda aroso","matosinhos aroso",676).
ligacao("matosinhos cirvunvalacao queima","matosinhos rotunda aroso",545).
ligacao("matosinhos rotunda aroso","matosinhos cirvunvalacao queima",545).
ligacao("matosinhos rotunda av republica","no matosinhos",1411).
ligacao("no matosinhos","matosinhos rotunda av republica",1411).
ligacao("matosinhos senhor matosinhos","ponte Leca sul",604).
ligacao("ponte Leca sul","matosinhos senhor matosinhos",604).
ligacao("matosinhos senhor matosinhos","no Leixoes Sul",687).
ligacao("no Leixoes Sul","matosinhos senhor matosinhos",687).
ligacao("farmacia parque","matosinhos camera norte",196).
ligacao("matosinhos camera norte","farmacia parque",196).
ligacao("matosinhos praia av republica","matosinhos foz leixoes",668).
ligacao("matosinhos foz leixoes","matosinhos praia av republica",668).
ligacao("matosinhos senhor matosinhos","matosinhos camera norte",542).
ligacao("matosinhos camera norte","matosinhos senhor matosinhos",542).
ligacao("farmacia ferreira da silva nortshoping","rotunda aep",221).
ligacao("rotunda aep","farmacia ferreira da silva nortshoping",221).
ligacao("circunvalacao alto do viso","rotunda aep",1337).
ligacao("rotunda aep","circunvalacao alto do viso",1337).
ligacao("circunvalacao curva blockbuster","circunvalacao alto do viso",1008).
ligacao("circunvalacao alto do viso","circunvalacao curva blockbuster",1008).
ligacao("circunvalacao montes burgos","circunvalacao curva blockbuster",225).
ligacao("circunvalacao curva blockbuster","circunvalacao montes burgos",225).
ligacao("circunvalacao no via norte","circunvalacao montes burgos",627).
ligacao("circunvalacao montes burgos","circunvalacao no via norte",627).
ligacao("circunvalacao amial","circunvalacao no via norte",795).
ligacao("circunvalacao no via norte","circunvalacao amial",795).
ligacao("circunvalacao Sao Tome","circunvalacao amial",359).
ligacao("circunvalacao amial","circunvalacao Sao Tome",359).
ligacao("circunvalacao IPO","circunvalacao Sao Tome",503).
ligacao("circunvalacao Sao Tome","circunvalacao IPO",503).
ligacao("circunvalacao roberto frias","circunvalacao IPO",567).
ligacao("circunvalacao IPO","circunvalacao roberto frias",567).
ligacao("circunvalacao IP1","circunvalacao roberto frias",377).
ligacao("circunvalacao roberto frias","circunvalacao IP1",377).
ligacao("farmacia Sao Joao","circunvalacao IPO",205).
ligacao("circunvalacao IPO","farmacia Sao Joao",205).
ligacao("vci A3","circunvalacao IP1",1030).
ligacao("circunvalacao IP1","vci A3",1030).
ligacao("porto Clerigos West","porto Clerigos East",136).
ligacao("porto Clerigos East","porto Clerigos West",136).
ligacao("vci no Paranhos","vci A3",755).
ligacao("vci A3","vci no Paranhos",755).
ligacao("farmacia da Prelada","vci curva da prelada",347).
ligacao("vci curva da prelada","farmacia da Prelada",347).
ligacao("vci no da prelada","vci no Paranhos",1514).
ligacao("vci no Paranhos","vci no da prelada",1514).
ligacao("circunvalacao no via norte","vci no da prelada",782).
ligacao("vci no da prelada","circunvalacao no via norte",782).
ligacao("porto Montes Burgos","circunvalacao montes burgos",424).
ligacao("circunvalacao montes burgos","porto Montes Burgos",424).
ligacao("farmacia dos Montes Burgos","porto Montes Burgos",196).
ligacao("porto Montes Burgos","farmacia dos Montes Burgos",196).
ligacao("porto Montes Burgos","vci no da prelada",538).
ligacao("vci no da prelada","porto Montes Burgos",538).
ligacao("porto Palacio de Cristal","rotunda boavista",1051).
ligacao("rotunda boavista","porto Palacio de Cristal",1051).
ligacao("porto Santo Antonio West","porto Palacio de Cristal",438).
ligacao("porto Palacio de Cristal","porto Santo Antonio West",438).
ligacao("porto Santo Antonio East","porto Santo Antonio West",215).
ligacao("porto Santo Antonio West","porto Santo Antonio East",215).
ligacao("porto Rua Carmo West","porto Santo Antonio East",103).
ligacao("porto Santo Antonio East","porto Rua Carmo West",103).
ligacao("porto Tribunal","porto Rua Carmo West",269).
ligacao("porto Rua Carmo West","porto Tribunal",269).
ligacao("porto Tribunal","porto Clerigos West",167).
ligacao("porto Clerigos West","porto Tribunal",167).
ligacao("porto Reitoria","porto Rua Carmo East",70).
ligacao("porto Rua Carmo East","porto Reitoria",70).
ligacao("porto Rua Carmo West","porto Rua Carmo East",155).
ligacao("porto Rua Carmo East","porto Rua Carmo West",155).
ligacao("porto Clerigos West","porto Reitoria",190).
ligacao("porto Reitoria","porto Clerigos West",190).
ligacao("porto Reitoria","porto Clerigos East",169).
ligacao("porto Clerigos East","porto Reitoria",169).
ligacao("porto Praca Carlos Alberto","porto Rua Carmo East",111).
ligacao("porto Rua Carmo East","porto Praca Carlos Alberto",111).
ligacao("porto Praca Carlos Alberto","porto Santo Antonio East",189).
ligacao("porto Santo Antonio East","porto Praca Carlos Alberto",189).
ligacao("farmacia Lemos","porto Praca Carlos Alberto",50).
ligacao("porto Praca Carlos Alberto","farmacia Lemos",50).
ligacao("farmacia Antiga da porta do Olival","porto Clerigos West",65).
ligacao("porto Clerigos West","farmacia Antiga da porta do Olival",65).
ligacao("porto Aliados Sul","porto Clerigos East",213).
ligacao("porto Clerigos East","porto Aliados Sul",213).
ligacao("porto Aliados Norte","porto Aliados Sul",836).
ligacao("porto Aliados Sul","porto Aliados Norte",836).
ligacao("farmacia dos Clerigos","porto Clerigos East",106).
ligacao("porto Clerigos East","farmacia dos Clerigos",106).
ligacao("porto Praca da Republica","porto Aliados Norte",286).
ligacao("porto Aliados Norte","porto Praca da Republica",286).
ligacao("porto Lapa","porto Praca da Republica",164).
ligacao("porto Praca da Republica","porto Lapa",164).
ligacao("rotunda boavista","porto Lapa",1330).
ligacao("porto Lapa","rotunda boavista",1330).
ligacao("farmacia do campo","porto Lapa",112).
ligacao("porto Lapa","farmacia do campo",112).
ligacao("porto Aliados Norte","vci no Paranhos",2086).
ligacao("vci no Paranhos","porto Aliados Norte",2086).
ligacao("porto Sao Nicolau","porto Aliados Sul",609).
ligacao("porto Aliados Sul","porto Sao Nicolau",609).
ligacao("ponte arrabida norte","porto Sao Nicolau",2338).
ligacao("porto Sao Nicolau","ponte arrabida norte",2338).
ligacao("porto Ponte do Freixo Sul","porto Ponte do Freixo Norte",654).
ligacao("porto Ponte do Freixo Norte","porto Ponte do Freixo Sul",654).
ligacao("vci Antas","vci A3",1493).
ligacao("vci A3","vci Antas",1493).
ligacao("vci Antas","porto Ponte do Freixo Norte",2244).
ligacao("porto Ponte do Freixo Norte","vci Antas",2244).
ligacao("circunvalacao Magalhaes Lemos","matosinhos cirvunvalacao queima",1602).
ligacao("matosinhos cirvunvalacao queima","circunvalacao Magalhaes Lemos",1602).
ligacao("rotunda aep","circunvalacao Magalhaes Lemos",652).
ligacao("circunvalacao Magalhaes Lemos","rotunda aep",652).
ligacao("ponte arrabida sul","ponte arrabida norte",223).
ligacao("ponte arrabida norte","ponte arrabida sul",223).
ligacao("vci Gerevide Gaia","porto Ponte do Freixo Sul",1461).
ligacao("porto Ponte do Freixo Sul","vci Gerevide Gaia",1461).
ligacao("vci Av Republica Gaia","vci Gerevide Gaia",1068).
ligacao("vci Gerevide Gaia","vci Av Republica Gaia",1068).
ligacao("vci No Telheira Gaia","vci Av Republica Gaia",1320).
ligacao("vci Av Republica Gaia","vci No Telheira Gaia",1320).
ligacao("gaia Rotunda Engenheiro Edgar Cardoso","vci No Telheira Gaia",1071).
ligacao("vci No Telheira Gaia","gaia Rotunda Engenheiro Edgar Cardoso",1071).
ligacao("vci no Gandara Gaia","vci No Telheira Gaia",889).
ligacao("vci No Telheira Gaia","vci no Gandara Gaia",889).
ligacao("vci no Coimbroes Gaia","vci no Gandara Gaia",1584).
ligacao("vci no Gandara Gaia","vci no Coimbroes Gaia",1584).
ligacao("farmacia Alves de Oliveira","vci Gerevide Gaia",352).
ligacao("vci Gerevide Gaia","farmacia Alves de Oliveira",352).
ligacao("farmacia Couto","vci Av Republica Gaia",129).
ligacao("vci Av Republica Gaia","farmacia Couto",129).
ligacao("farmacia Coimbroes","vci no Coimbroes Gaia",197).
ligacao("vci no Coimbroes Gaia","farmacia Coimbroes",197).
ligacao("vci ArrabidaShoping West gaia","ponte arrabida sul",894).
ligacao("ponte arrabida sul","vci ArrabidaShoping West gaia",894).
ligacao("vci ArrabidaShoping East gaia","ponte arrabida sul",899).
ligacao("ponte arrabida sul","vci ArrabidaShoping East gaia",899).
ligacao("gaia Rotunda Engenheiro Edgar Cardoso","vci ArrabidaShoping East gaia",1240).
ligacao("vci ArrabidaShoping East gaia","gaia Rotunda Engenheiro Edgar Cardoso",1240).
ligacao("vci no Coimbroes Gaia","vci ArrabidaShoping West gaia",1241).
ligacao("vci ArrabidaShoping West gaia","vci no Coimbroes Gaia",1241).
ligacao("vci ArrabidaShoping West gaia","vci ArrabidaShoping East gaia",277).
ligacao("vci ArrabidaShoping East gaia","vci ArrabidaShoping West gaia",277).
ligacao("ponte Dao Luis Norte","porto Sao Nicolau",408).
ligacao("porto Sao Nicolau","ponte Dao Luis Norte",408).
ligacao("ponte Dao Luis Sul","ponte Dao Luis Norte",185).
ligacao("ponte Dao Luis Norte","ponte Dao Luis Sul",185).
ligacao("ponte Dao Luis Norte","porto Ponte do Freixo Norte",2434).
ligacao("porto Ponte do Freixo Norte","ponte Dao Luis Norte",2434).
ligacao("ponte Dao Luis Sul","vci Av Republica Gaia",1501).
ligacao("vci Av Republica Gaia","ponte Dao Luis Sul",1501).
ligacao("passeio Alegre porto","Forte da foz",670).
ligacao("Forte da foz","passeio Alegre porto",670).
ligacao("farmacia do Passeio Alegre","passeio Alegre porto",187).
ligacao("passeio Alegre porto","farmacia do Passeio Alegre",187).
ligacao("passeio Alegre porto","ponte arrabida norte",2183).
ligacao("ponte arrabida norte","passeio Alegre porto",2183).
ligacao("Praca do Imperio porto","Av Boavista com Gomes da Costa",1636).
ligacao("Av Boavista com Gomes da Costa","Praca do Imperio porto",1636).
ligacao("Sra da Luz foz","Praca do Imperio porto",526).
ligacao("Praca do Imperio porto","Sra da Luz foz",526).
ligacao("castelo queijo","Sra da Luz foz",1952).
ligacao("Sra da Luz foz","castelo queijo",1952).
ligacao("Forte da foz","Sra da Luz foz",463).
ligacao("Sra da Luz foz","Forte da foz",463).
ligacao("castelo queijo","Av Boavista com Gomes da Costa",2811).
ligacao("Av Boavista com Gomes da Costa","castelo queijo",2811).
ligacao("vci boavista","Av Boavista com Gomes da Costa",531).
ligacao("Av Boavista com Gomes da Costa","vci boavista",531).
ligacao("farmacia da foz","Sra da Luz foz",157).
ligacao("Sra da Luz foz","farmacia da foz",157).




